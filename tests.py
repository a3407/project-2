import pytest
import numpy as np
from OptimizationProblemSolverBase import QuasiNewtonBase, NewtonNotConverge, Problem
from chebyquad_problem import gradchebyquad, chebyquad
from NewtonMethods import  Broyden_bad,Broyden_good,SR1,DFP,BFGS2 
from NewtonMethods import LineSearch

###helper functions
def f(x):
    return x[0]**2 + x[1]**2 + 2

def g(x,h=0.001):
    return np.array([2*x[0],2*x[1]])


def Rosenbrock(x):
    return 100*(x[1]-x[0])**2 + (1-x[0])**2

def a(self,x):
    return 1

### Tests

solvers = [
        QuasiNewtonBase,
        Broyden_bad,
        Broyden_good,
        SR1,
        DFP,
        BFGS2
        ]

stopping_conditions = [
        None
        ]

LineSearch = [
       LineSearch.get_exact_search(),
       LineSearch.get_goldstein(),
       LineSearch.get_powell_wolfe(),
       LineSearch.get_powell_wolfe(sigma = 0.1, rho = 0.01),
        ]

starting_points = [
        np.array([0.9,0.0]),
#        np.array([1.0,1.0]),
        np.array([0.0,0.0]),
        np.array([0.0,0.9]),
        np.array([0.9,0.9]),
        np.array([-0.9,0.0]),
        np.array([-0.9,-0.9]),
        np.array([-0.9,0.9]),
        np.array([1.0,-1.0]),
        ]

functions = [
        (Rosenbrock,np.array([1,1]),None,1e-6),
        (f,np.array([0,0]),g,1e-4),
        ]

@pytest.mark.parametrize("solver",solvers)
@pytest.mark.parametrize("func,res,grad,tol",functions)
@pytest.mark.parametrize("x_0",starting_points)
@pytest.mark.parametrize("stopping_cond",stopping_conditions)
@pytest.mark.parametrize("line_search",LineSearch)
def test_methodWorking(solver,x_0,func,grad,res,tol,stopping_cond, line_search):

    
    problem = Problem(func,grad)
    optimizer = solver(problem)
    if not line_search == None:
        optimizer.alpha = line_search
    if not stopping_cond == None:
        optimizer.check_stop_cond = stopping_cond
    
    sol = optimizer.optimize(x_0, 2e3,TOL = tol)  
    print(sol)
    assert np.allclose(sol,res, atol=1e-2), f"sol_val:{sol}"


@pytest.mark.parametrize("solver_class",solvers)
@pytest.mark.parametrize("problem_var",[1,'test',0.2,lambda x: x[2] + x[1]])
def test_ProbelmTypeError(problem_var, solver_class):
    with pytest.raises(TypeError):
        assert solver_class(problem_var) 

def test_NewtonNotConvergeError():
    pass


