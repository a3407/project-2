from OptimizationProblemSolverBase import QuasiNewtonBase
import numpy as np
import scipy.optimize as opt


class LineSearch:
    @staticmethod
    def get_exact_search():

        def exact_search(self,x_k,s_k):
            f = self.problem.f

            phi = lambda a : f(x_k + a*s_k)

            return opt.minimize(phi, 1).x

        return exact_search


    @staticmethod
    def get_goldstein(a = 1, sigma = 1e-2, rho = 0.9, h=1e-5, max_itt = 100):
        
        def goldstein(self, x_k, s_k):
            f = self.problem.f
            phi = lambda ai : f(x_k + ai*s_k)
            dphi = lambda ai :(phi(ai+h)-phi(ai-h))/(2*h)

            a_minus = a
            for _ in range(max_itt):
                if not  (phi(a_minus) >= phi(0) + sigma*a_minus*dphi(0)): #armijo condition
                    break
                a_minus = a_minus/2
            else:
                raise ValueError
        
            a_plus = a_minus
            for _ in range(max_itt):
                if (phi(a_plus) >= phi(0) + sigma*a_plus*dphi(0)): #armijo condition
                    break
                a_plus = 2 * a_plus
            else:
                raise ValueError

        
            a_fin = a_minus 
            for _ in range(max_itt):
                if phi(a_minus) <= phi(0) + (1-sigma)*a_minus*dphi(0):
                    break
                a_zero = (a_plus + a_minus)/2
                if (phi(a_zero) <= phi(0) + sigma*a_zero*dphi(0)):
                    a_minus = a_zero
                else:
                    a_plus = a_zero
                a_fin = a_minus
            else:
                return a_fin
                raise ValueError
            return a_fin

        return  goldstein
    

    @staticmethod
    def get_powell_wolfe(a = 1, sigma = 1e-2, rho = 0.9, h=1e-5, max_itt = 100):
        
        def powell_wolfe(self, x_k, s_k):
            f = self.problem.f
            phi = lambda ai : f(x_k + ai*s_k)
            dphi = lambda ai :(phi(ai+h)-phi(ai-h))/(2*h)

            a_minus = a
            for _ in range(max_itt):
                if not  (phi(a_minus) > phi(0) + sigma*a_minus*dphi(0)): #armijo condition
                    break
                a_minus = a_minus/2
            else:
                raise ValueError
        
            a_plus = a_minus
            for _ in range(max_itt):
                if not (phi(a_plus) <= phi(0) + sigma*a_plus*dphi(0)): # condition 2 
                    break
                a_plus = 2*a_plus
            else:
                raise ValueError

            a_fin = a_minus 
            for _ in range(max_itt):
                if not (dphi(a_minus) < rho*dphi(0)):
                    break
                a_zero = (a_plus + a_minus)/2
                if (phi(a_zero) <= phi(0) + sigma*a_zero*dphi(0)):
                    a_minus = a_zero
                else:
                    a_plus = a_zero
                a_fin = a_minus
            else:
                raise ValueError
            return a_fin
        
        return powell_wolfe
            




class Broyden_good(QuasiNewtonBase):

    def update_hessian(self, x_k, s_k,h=1e-5):
        if self.H is None:
            QuasiNewtonBase.update_hessian(self,x_k,s_k,h)
        else:
            H_k = self.H
            y_k = self.problem.J(x_k)
            y_k = np.reshape(y_k,(-1,1))
            x_k = np.reshape(x_k,(-1,1))
            s_k = np.reshape(s_k,(-1,1))
            
            self.H += ((s_k-H_k@y_k)@s_k.T@H_k)/(s_k.T@H_k@y_k)


class Broyden_bad(QuasiNewtonBase):

    def update_hessian(self, x_k, s_k,h=1e-5):
        if self.H is None:
            QuasiNewtonBase.update_hessian(self,x_k,s_k,h)
        else:
            H_k = self.H
            y_k = self.problem.J(x_k)
            y_k = np.reshape(y_k,(-1,1))
            x_k = np.reshape(x_k,(-1,1))
            s_k = np.reshape(s_k,(-1,1))
            self.H += (s_k-H_k@y_k)@y_k.T/(y_k.T@y_k)



            
class SR1(QuasiNewtonBase):

    def update_hessian(self, x_k, s_k,h=1e-5):
        if self.H is None:
            QuasiNewtonBase.update_hessian(self,x_k,s_k,h)
        else:
            H_k = self.H
            y_k = self.problem.J(x_k)
            y_k = np.reshape(y_k,(-1,1))
            x_k = np.reshape(x_k,(-1,1))
            s_k = np.reshape(s_k,(-1,1))
            self.H += ((s_k-H_k@y_k)@((s_k-H_k@y_k).T))/((s_k-H_k@y_k).T@y_k)
            

class DFP(QuasiNewtonBase):

    def update_hessian(self, x_k, s_k,h=1e-5):
        if self.H is None:
            QuasiNewtonBase.update_hessian(self,x_k,s_k,h)
        else:
            H_k = self.H
            y_k = self.problem.J(x_k)
            y_k = np.reshape(y_k,(-1,1))
            x_k = np.reshape(x_k,(-1,1))
            s_k = np.reshape(s_k,(-1,1))
            self.H += (s_k@s_k.T)/(s_k.T@y_k)-(H_k@y_k@y_k.T@H_k)/(y_k.T@H_k@y_k)



class BFGS2(QuasiNewtonBase):

    
    def update_hessian(self, x_k, s_k,h=1e-5):
        if self.H is None:
            QuasiNewtonBase.update_hessian(self,x_k,s_k,h)
        else:
            H_k = self.H
            dim = H_k.shape[0] 
            I = np.identity(dim)
            y_k = self.problem.J(x_k)
            y_k = np.reshape(y_k,(-1,1))
            x_k = np.reshape(x_k,(-1,1))
            s_k = np.reshape(s_k,(-1,1))
            self.H = (I-(s_k@y_k.T)/(y_k.T@s_k))@H_k@(I-(y_k@s_k.T)/(y_k.T@s_k))+(s_k@s_k.T)/(y_k.T@s_k)
