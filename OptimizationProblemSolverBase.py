# -*- coding: utf-8 -*-
import numpy as np

class NewtonNotConverge(Exception):
    def __init__(self,expr):
        self.expr = expr
    def __str__(self):
        return str(self.expr)

class HessianCalcError(Exception):
    def __init__(self,expr):
        self.expr = expr
    def __str__(self):
        return str(self.expr)

class Problem:

    def __init__(self, f, J = None):
        self.f = f
        if J == None:
            self.J = self.finite_diff
        else:
            self.J = J

    def finite_diff(self,x,h = 1e-5):
        one_hot = np.zeros_like(x)
        one_hot[0] = 1.0
        gradients = np.array([(self.f(x+h*np.roll(one_hot,i))-self.f(x-h*np.roll(one_hot,i)))/(2*h) for i in range(len(x))])
        return gradients

class QuasiNewtonBase:

    def __init__(self, problem):
        #TODO: Test that if the problem is not a subclass of the problem then we throw an error
        if not isinstance(problem,Problem):
            raise TypeError("passed problem variable is not instance of the Problem class")
        self.problem = problem
        self.H = None
    
    def optimize(self, x_0, max_steps = 1e3, TOL = 1e-5):
        final_val = None
        for val in self.newton_step(x_0,max_steps,TOL):
            final_val = val
        return final_val


    def newton_step(self, x_0,max_steps = 1e3, TOL = 1e-5):
        x_k = x_0.copy()
        self.update_hessian(x_k,None)
        g = self.problem.J

        print(g(x_k))
        if np.allclose(g(x_k),np.zeros_like(x_k)):
            yield x_k
            return

        for _ in range(int(max_steps)):
            s_k = -self.H @ g(x_k)
            #print(f"sk:{s_k}")
            alpha = self.alpha(self,x_k,s_k)
            alphas= alpha*s_k
            x_k += alphas
            yield x_k.copy()
            if self.check_stop_cond(x_k,alphas,TOL):
                break
            self.update_hessian(x_k,s_k)
        else:
            raise NewtonNotConverge(f"Newton method did not converge, ended at point:{x_k}")


    
    def update_hessian(self, x_k, s_k,h=1e-5):
        """
        Calculate a new Nessian based on the current positon x_k
        and store in self.H
        """
        one_hot = np.zeros_like(x_k)
        one_hot[0] = 1.0
        f_prime = self.problem.J
        point = f_prime(x_k)
        hess = np.array([(f_prime(x_k + h*np.roll(one_hot,i)) - point)/h for i in range(len(x_k))])
        hess = 0.5 * (hess + hess.T)
        try:
            self.H = np.linalg.inv(hess)
        except np.linalg.LinAlgError:
            raise HessianCalcError(f"Unable to invert the Hessian, \n, {hess}")

    
    def check_stop_cond(self, x_k, s_k, TOL):
        """
        check if the newton method has converged
        """
        return np.linalg.norm(s_k) < TOL #and np.linalg.norm(self.problem.J(x_k))<TOL
    
    @staticmethod
    def alpha(self, x_k, s_k):
        return 1
    
if __name__ == "__main__":
    import matplotlib.cm as cm
    import matplotlib.pyplot as plt


    delta = 0.05
    x = np.arange(-0.5, 2.0, delta)
    y = np.arange(-1.5, 4.0, delta)
    X, Y = np.meshgrid(x, y)
    ZR1 = (1-X)**2 #implementing the rosenberg function in the plot
    ZR2 = (100*(Y-X**2)**2)
    ZR = (ZR1 +ZR2)



    fig, ax = plt.subplots()
    CS = ax.contour(X, Y, ZR,np.logspace(0.1,12,50),colors='k')
    ax.clabel(CS, inline=True, fontsize=10)
    ax.set_title('Rosenbrocks Gleichung')


    fig, ax = plt.subplots()
    CS = ax.contour(X, Y, ZR,np.logspace(0.1,20,50),colors='k')
    ax.clabel(CS, inline=True, fontsize=10)
    ax.set_title('Schritte der Goldstein Funktion')


    #set the points 
    f = lambda x : 100 * (x[1]-x[0]**2)**2 + (1-x[0])**2
    prob = Problem(f)
    solv = QuasiNewtonBase(prob)
    points = np.array([i for i in solv.newton_step(np.array([-0.4,-1.4]))])
    x1 = points[:,0] #test set of points
    y1 = points[:,1]
    plt.plot(x1, y1, '--ok', color='black',
         markersize=4, linewidth=1,);
    plt.show()
