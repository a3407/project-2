from chebyquad_problem import chebyquad, gradchebyquad  
from OptimizationProblemSolverBase import Problem,QuasiNewtonBase
from NewtonMethods import LineSearch,BFGS2
from scipy.optimize import fmin_bfgs
import numpy as np

for n in [4,8,11]:
    print(f"--------------\nn={n}")
    prob = Problem(chebyquad,gradchebyquad)
    x0=np.linspace(0,1,n)
    solv = BFGS2(prob)
    solv.alpha = LineSearch.get_exact_search()
    x = solv.optimize(x0,TOL = 1e-4)
    x_sci = fmin_bfgs(chebyquad,x0,gradchebyquad)
    print(f"our  :x={x}")
    print(f"scipy:x={x_sci}")
    
print("Evaluation Of Hessian")
prob = Problem(chebyquad,gradchebyquad)
solv = BFGS2(prob)
solv.alpha = LineSearch.get_exact_search()
x0=np.linspace(0,1,4)
for i,x in enumerate(solv.newton_step(x0,TOL = 1e-4)):
    print(f"Step:{i}\nx_k:{x}")
    print( solv.H )
    

